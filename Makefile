CC = gcc
LDFLAGS = -framework SDL2
MAIN = Tp2
CCFLAGS = `sdl-config --cflags`

.PHONY: all clean

all: $(MAIN)

$(MAIN): $(MAIN).o
	$(CC) $(LDFLAGS) $< -o $@

$(MAIN).o: $(MAIN).c
	$(CC) -c $(CCFLAGS) $< -o $@

clean:
	rm -f *.o $(MAIN)
