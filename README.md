# Travail pratique 2

## Description

Décrire ici le projet en quelque phrases.
Mentionner le contexte (cours, sigle, université, etc.)

 - Le projet conciste à faire un jeu en SDL en C. Pour le moment,
   on ne fait que le menu.
 - Dans le cadre du cours Construction et maintenance de logiciels.
 - INF3135-10
 - Université du Québec à Montréal
## Auteur(s)

- Renaldo Astorga (ASTR21079008)


## Dépendances

Indiquez ici toutes librairies que vous avez utilisées et dont votre projet
dépend. En particulier, indiquez les versions de chacune de ces librairies (par
exemple SDL 2.0, Doxygen 1.8.10, etc.).
Les dépendances sont:
- SDL 2.0
- SDL2/SDL_Image.h
- stdbool.h

## Fonctionnement

Pas pu l'exécuter, car difficulté et problème:
Essaye sur windows, problème à l'afficher: sdl2.dll n'existe pas?
Il ne faut que faire exécuter le MakeFile sur le bash.

Expliquez comment faire fonctionner votre projet :

- Comment le compiler;
- Comment l'exécuter;
- Comment le tester;
- Comment générer la documentation:
	Pour générer la documentation il faut ouvrir le readMe qui correspond au projet.
- etc.

## Plateformes supportées

Indiquez ici les plateformes sur lesquelles vous avez testé le projet, avec la
version exacte (par exemple MacOS 10.10.5, Debian 8.3, etc.) Dans un scénario
idéal, vous devriez tester au moins deux plateformes, mais aucune pénalité ne
sera appliquée si vous n'en testez qu'une seule (en autant qu'il s'agisse d'une
plateforme Unix!).
Plateformes supportées: 
- Unix: Debian 8.3 [ NON]
- Windows [Non]


## Contenu du projet

Décrivez brièvement chacun des répertoires et modules contenus dans le
projet. Utilisez le format suivant :

- `Tp2` : Elle contient le code pour le menu principal du jeu;
- Le fichier `image` : contient les images du menu;
- etc.

## Division des tâches



- [X] Tâche 1 (Renaldo : Faire le menu avant le 18 mars 23h59):Pas pu exécuter
- [ ] Tâche 2 (Renaldo: Faire fonctionner le menu)
- [ ] Tâche 3 (Renaldo: Faire les sprites)
- [ ] Tâche 4 (nom du responsable de la tâche)
- etc.

(Un X indique que la tâche est complétée, alors que des crochets vides
indiquent que la tâche n'est pas complétée)

## Références

-http://www.supinfo.com/articles/single/649-comment-installer-sdl-20-codeblocks-windows REFERENCE QUI POURRAIT ETRE UTILE SI TROUVER AU DEBUT.
-http://lazyfoo.net/SDL_tutorials/
- netprof.fr: SITE UTILE, MAIS DATÉ.
http://www.thales.math.uqam.ca/~blondin/fr/inf3135
## Statut

Indiquer si le projet est complété, si tout est fonctionnel:
-Le menu est complété,  pas encore  fonctionnel.
-load du sprite non fonctionnel
-bouton de mouvement non fonctionnel
- Problème de compatibilité, de vue fonctionnel et de temps.
