#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL.h> // avec windows et il faut faire de la modification ds le compilateur.
#include <stdbool.h>
//#include "spritesheet.h"

// Constants
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SPRITE_N_LIGNE = 4;
const int SPRITE_N_COLONNE = 5;
const int SPRITE_N_FRAMES = 20;



/*
* Renaldo Astorga ASTR21079008
*	INF3135-10
*
* Classe qui contient le menu du jeu.
* Il faut cliquer pour accèder au fonction
* du menu. Les boutons de difficulté ne sont
* pas encore fonctionnel, ainsi que le bouton jouer.
* Le code compile bien. Toujours pas fonctionnel.
*
*
*/
struct Appli
{
	SDL_Window* gWindow;

	SDL_Surface *pFenetre, *pJouer, *pDifficulte, *pQuitter, *pDifficile, *pFacile, *pIntermediaire, *pTitre;

	SDL_Rect posJouer, posDifficulte, posFacile, posIntermediaire, posDifficile, posQuitter, positionSouris, posTitre; // posPersonnage;
	SDL_Event event;
	int continuer;

    //SDL_Renderer renderer;
};


bool init(struct Appli *app);
bool loadMedia(struct Appli *app);
void initEmplacementImage(struct Appli *app);
void leMenu(struct Appli *app);
void lesBoutons(struct Appli *app);
void lesNiveauDeDifficulte(struct Appli *app);
void close(struct Appli *app);
//void mouvement(struct Appli *app);
//bool loadSpritesheet(struct Application *app);


int main (int argc, char** argv )
{
	struct Appli app = {NULL, NULL, NULL,  NULL, NULL, NULL, NULL, NULL, NULL,  NULL,
						NULL, NULL, NULL, NULL, NULL,  NULL, NULL, NULL, 1};




    if (!init(&app))
    {
        printf( "Failed to initialize!\n" );
        return 1;
    }
    else if (!loadMedia(&app))
    {
        printf( "Failed to load image!\n" );
        return 2;
    }
    else {
		leMenu(&app);
		SDL_Delay(360000);
    }
	close(&app);
	return EXIT_SUCCESS;

}





/**
* La méthode loadSpritesheet prend en paramètre un pointuer app
* d'une structure Appli. Elle fait le chargement des fichiers
* images. Si l'image est null, elle retourne un message d'erreur
* et devient false.
*
* @param app qui contient tous les paramètre du programme.
* @return un booleen .
*
*
**/
/*
bool loadSpritesheet(struct Application *app) {
    app->sprite = Spritesheet_create(SPRITE_NOM,
        SPRITE_N_LIGNE, SPRITE_N_COLONNE, SPRITE_N_FRAMES,
        app->renderer);
    if (!app->spritesheet) {
        printf( "Failed to load sprite sheet texture!\n" );
        return false;
    }
    return true;
}
*/

/**
* La méthode init prend en paramètre un pointuer app
* d'une structure Appli. Elle fait le création de la fenêtre.
* Elle affiche un message d'erreur soit si elle n'a pas pu être
* initialisée ou si la fenêtre n'a pas été crée en laissant la
* variable reussite à false.
* Sinon elle retourne la variable reussite.
*
* @param app qui contient tous les paramètre du programme.
* @return un booleen reussite.
*/
bool init(struct Appli *app)
{
	bool reussite = true;

	//initialise SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL n'a pas pu etre initialise! SDL_Erreur: %s\n", SDL_GetError());
		reussite = false;
	}
	else
	{
		app -> gWindow = SDL_CreateWindow("Bob, l'aventurier: Menu", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_HEIGHT, SCREEN_WIDTH,
                                     SDL_WINDOW_SHOWN);
	//	SDL_WM_SetCaption("Jeux: Bob, l'aventurier", NULL);

		if(app -> gWindow == NULL)
		{
			printf("La fenetre n'a pas pu etre creer! SDL_Erreur: %s\n", SDL_GetError());
			reussite = false;
		}
		else
		{
			app -> pFenetre = SDL_GetWindowSurface (app -> gWindow);

			SDL_FillRect(app -> pFenetre, NULL, SDL_MapRGB(app ->pFenetre->format, 61, 215, 241));
		}
	}
	return reussite;
}

/**
* La méthode initEmplacementImage prend en paramètre un pointuer app
* d'une structure Appli. Elle ne fait que initialiser les positions
* des images.
*
* @param app qui contient tous les paramètre du programme.
*
*/
void initEmplacementImage(struct Appli *app)
{
	app -> posJouer.x = 10;
	app -> posJouer.y = 500;

	app -> posDifficulte.x = 10;
	app -> posDifficulte.y = 584;

	app -> posFacile.x = 317;
	app -> posFacile.y = 500;

	app -> posIntermediaire.x = 317;
	app -> posIntermediaire.y = 585;

	app -> posDifficile.x = 317;
	app -> posDifficile.y = 673;

	app -> posQuitter.x = 10;
	app -> posQuitter.y = 667;

	app -> posTitre.x = 82;
	app -> posTitre.y = 50;
}


/**
* La méthode mouvement prend en paramètre un pointuer app
* d'une structure Appli. Elle permet au deplacement du personnage.
*
* @param app qui contient tous les paramètre du programme.
*
*/
/*
void mouvement(struct Appli *app)
{
	SDL_EnableKeyRepeat(10, 10);
	while(continue)
	{
		SDL_PollEvent(&event);
		switch(event.type)
		{

			case SDL_KEYDOWN:
			switch(event.key.keysym.sym)
			{
				case SDLK_w:
				posPersonnage.y -= 5;
				//block le PollEvent
				SDL_WaitEvent(&event);
				break;

				case SDLK_s:
				posPersonnage.y +=5;
				//block le PollEvent
				SDL_WaitEvent(&event);
				break;

				case SDLK_d:
				posPeronnage.x += 5;
				//block le PollEvent
				SDL_WaitEvent(&event);
				break;

				case SDLK_a:
				posPersonnage.x -= 5;
				//block le PollEvent
				SDL_WaitEvent(&event);
				break;

				case SDLK_SPACE:

				break;
		}
		break;

	}

	SDL_BlitSurface(personnage, NULL, pFenetre, &posPersonnage);
	SDL_Flip(pFenetre);

 }

}
*/



/**
* La méthode loadMedia prend en paramètre un pointuer app
* d'une structure Appli. Elle fait le chargement des fichiers
* images. Si l'image est null, elle retourne un message d'erreur
* et la  variable marche devient false.
* Sinon elle initialise les positions des images à l'aide de la
* méthode initEmplacementImage, affiche les images et retourne
* la variable marche.
*
* @param app qui contient tous les paramètre du programme.
* @return un booleen marche.
*/




bool loadMedia(struct Appli *app)
{
	bool marche = true;
/*
            Version pour essayer avec windows: ne fonctionne pas
	app -> pTitre = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\titre.png");
	app -> pJouer = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\jouer.png");
	app -> pDifficulte = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\difficulte.png");
	app -> pFacile = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\facile.png");
	app -> pIntermediaire = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\intermediaire.png");
	app -> pDifficile = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\difficile.png");
	app -> pQuitter = IMG_Load("C:\\Users\\renaldo\\Desktop\\inf3135-hiv2016-tp2\\image\\quitter.png");

*/

	app -> pTitre = IMG_Load("image/titre.png");
	app -> pJouer = IMG_Load("image/jouer.png");
	app -> pDifficulte = IMG_Load("image/difficulte.png");
	app -> pFacile = IMG_Load("image/facile.png");
	app -> pIntermediaire = IMG_Load("image/intermediaire.png");
	app -> pDifficile = IMG_Load("image/difficile.png");
	app -> pQuitter = IMG_Load("image/quitter.png");

	if(( app -> pTitre == NULL ||
        app -> pJouer == NULL || app -> pDifficulte== NULL ||
	    app -> pFacile == NULL || app -> pIntermediaire == NULL ||
	    app -> pDifficile == NULL || app -> pQuitter == NULL) )
	{
		printf("Image non disponible %s! SDL_Erreur:%s\n","EXTENSION_IMAGE.EXT", SDL_GetError());
		marche = false;
		return marche;
	}else
	{
		initEmplacementImage(app);
		SDL_BlitSurface(app -> pTitre, NULL, app -> pFenetre, &(app -> posTitre));
		SDL_BlitSurface(app -> pJouer, NULL, app -> pFenetre, &(app -> posJouer));
		SDL_BlitSurface(app -> pDifficulte, NULL, app -> pFenetre, &(app -> posDifficulte));
		SDL_BlitSurface(app -> pQuitter, NULL, app -> pFenetre, &(app -> posQuitter));

		SDL_UpdateWindowSurface(app -> gWindow);
	}
	return marche;
}


/**
* La méthode lesBoutons prend en paramètre un pointuer app
* d'une structure Appli. Elle fait boucler la méthode lesBoutons.
* Elle arrête de boucler lorsque app -> continue =0.
*
* @param app qui contient tous les paramètre du programme.
*
*/
void leMenu(struct Appli *app)
{

	while (app-> continuer)
	{
		lesBoutons(app);
	}

}

/**
* La méthode lesBoutons prend en paramètre un pointuer app
* d'une structure Appli. Elle fait la gestion des boutons.
* Plus précisement, elle gèrer les événements de la souris.
*
* @param app qui contient tous les paramètre du programme.
*
*/
void lesBoutons(struct Appli *app)
{
	SDL_PollEvent( &(app -> event));
	switch(app -> event.type)
	{
		case SDL_QUIT:
			app -> continuer = 0;
			break;
		case SDLK_ESCAPE:
			app -> continuer = 0;
			break;
		case SDL_MOUSEBUTTONUP:
			if(app -> event.button.button == SDL_BUTTON_LEFT)
			{
				app -> positionSouris.x = app -> event.button.x;
				app -> positionSouris.y = app -> event.button.y;
			}
			if(app -> event.button.x > app -> posJouer.x && app -> event.button.x < (app -> posJouer.x + 201)
				&& app -> event.button.y > app -> posJouer.y && app -> event.button.y < (app -> posJouer.y + 54))			{
				app -> continuer = 0;
			}
			else if(app -> event.button.x > app -> posDifficulte.x && app -> event.button.x < (app -> posDifficulte.x + 207)
				&& app -> event.button.y > app -> posDifficulte.y && app -> event.button.y < (app -> posDifficulte.y + 53))

			{
				lesNiveauDeDifficulte(app);

			}
			else if(app -> event.button.x > app -> posQuitter.x && app ->event.button.x < (app -> posQuitter.x + 210)
				&& app -> event.button.y > app -> posQuitter.y && app ->event.button.y < (app -> posQuitter.y + 59))
			{
				app -> continuer = 0;
			}
			break;
	}

}

/**
* La méthode lesNiveauDeDifficulte prend en paramètre un pointuer
* app d'une structure Appli. Future methode qui permettra à gérer
* la difficultée du jeu.
*
* @param app qui contient tous les paramètre du programme.
*
*/
void lesNiveauDeDifficulte(struct Appli *app)
{

	SDL_BlitSurface(app -> pFacile, NULL, app -> pFenetre, &(app -> posFacile));
	SDL_BlitSurface(app -> pIntermediaire, NULL, app -> pFenetre, &(app -> posIntermediaire));
	SDL_BlitSurface(app -> pDifficile, NULL, app -> pFenetre, &(app -> posDifficile));
	SDL_UpdateWindowSurface(app -> gWindow);
}

/**
* La méthode close prend en paramètre un pointuer app de d'une
* structure Appli. Elle permet de libérer la mémoire: liberer
* les images, les pointeurs, la fenêtre et quit le SDL.
*
* @param app qui contient tous les paramètre du programme.
*
*/
void close(struct Appli *app)
{
	SDL_FreeSurface (app -> pTitre);
	SDL_FreeSurface (app -> pJouer);
	SDL_FreeSurface (app -> pDifficulte);
	SDL_FreeSurface (app -> pFacile);
	SDL_FreeSurface (app -> pIntermediaire);
	SDL_FreeSurface (app -> pDifficile);
	SDL_FreeSurface (app -> pQuitter);

	app -> pTitre = NULL;
	app -> pJouer = NULL;
	app -> pDifficulte = NULL;
	app -> pFacile = NULL;
	app -> pIntermediaire = NULL;
	app -> pDifficile = NULL;
	app -> pQuitter = NULL;

	SDL_DestroyWindow (app -> gWindow);

	app -> gWindow = NULL;

	SDL_Quit();

}
