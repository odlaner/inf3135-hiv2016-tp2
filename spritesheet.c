/**
*
*Droit appartient à Alexandre Blondin Massé
*Site:
*http://www.thales.math.uqam.ca/~blondin/fr/inf3135 
**/

#include "spritesheet.h"

Spritesheet *Spritesheet_create(const char *filename,
                                int numRows,
                                int numColumns,
                                int numFrames,
                                SDL_Renderer* renderer) {
	Spritesheet *spritesheet = (Spritesheet*)malloc(sizeof(Spritesheet));
    spritesheet->numRows = numRows;
    spritesheet->numColumns = numColumns;
    spritesheet->numFrames = numFrames;
	spritesheet->texture = NULL;
	spritesheet->renderer = renderer;
	SDL_Surface *loadedSurface = IMG_Load(filename);
	if (loadedSurface == NULL) {
		printf("Unable to load image %s! SDL_image Error: %s\n",
               filename, IMG_GetError());
	} else {
        spritesheet->texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (spritesheet->texture == NULL) {
			printf("Unable to create texture from %s! SDL Error: %s\n",
                   filename, SDL_GetError());
        }
        spritesheet->spriteWidth = loadedSurface->w / numColumns;
        spritesheet->spriteHeight = loadedSurface->h / numRows;
	}
    return spritesheet;
}

void Spritesheet_delete(Spritesheet *spritesheet) {
    //TODO
}

void Spritesheet_render(Spritesheet *spritesheet, int x, int y, int frame) {
    int srcx = spritesheet->spriteWidth * (frame % spritesheet->numColumns);
    int srcy = spritesheet->spriteHeight * (frame / spritesheet->numColumns);
    SDL_Rect srcrect = {srcx, srcy, spritesheet->spriteWidth, spritesheet->spriteHeight};
    SDL_Rect dstrect = {x, y, spritesheet->spriteWidth, spritesheet->spriteHeight};
	SDL_RenderCopy(spritesheet->renderer, spritesheet->texture, &srcrect, &dstrect);
}
