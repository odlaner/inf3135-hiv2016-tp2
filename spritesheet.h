#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>

typedef struct {
    int numColumns;         // Le nombre de colonnes
    int numRows;            // Le nombre de rangees
    int numFrames;          // Le nombre de sprites
    int spriteWidth;        // La largeur d'une sprite
    int spriteHeight;       // La hauteur d'une sprite
    SDL_Renderer *renderer; // Le moteur de rendu des images
    SDL_Texture *texture;   // La texture associee a la feuille
} Spritesheet;

/**
 * Cree une feuille de sprites à partir d'un fichier donne.
 *
 * Note : Le nombre de sprites doit etre precise pour le cas
 * ou la feuille n'est pas pleine (la derniere rangee).
 *
 * @param filename   Le nom du fichier contenant la feuille
 * @param numRows    Le nombre de rangees dans la feuille
 * @param numRows    Le nombre de colonnes dans la feuille
 * @param numFrames  Le nombre sprites
 * @param renderer   Le moteur de rendu sur lequel la sprite
 * @return           La feuille de sprites
 */
Spritesheet *Spritesheet_create(const char *filename,
                                int numRows, int numColumns,
                                int numFrames, SDL_Renderer* renderer);

/**
 * Supprime la feuille de sprites.
 *
 * @param filename  La feuille de sprites a supprimer
 */
void Spritesheet_delete(Spritesheet *spritesheet);

/**
 * Affiche la sprite donnee en position (x,y).
 *
 * @param x      L'abscisse du coin superieur gauche de la sprite
 * @param y      L'ordonnee du coin superieur gauche de la sprite
 * @param frame  Le numero de sprite a afficher
 */
void Spritesheet_render(Spritesheet *spritesheet, int x, int y, int frame);

#endif
